package com.github.factory;

public class Pig implements Animal {
    @Override
    public void play() {
        System.out.println("Pig.play");
    }
}
