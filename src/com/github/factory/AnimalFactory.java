package com.github.factory;

public class AnimalFactory {

    public static Animal getAnimal(Class<? extends Animal> clazz) {
        Animal animal = null;
        try {
            animal = (Animal) Class.forName(clazz.getName()).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return animal;
    }

    public static void main(String[] args) {
        Animal dog = AnimalFactory.getAnimal(Dog.class);
        Animal pig = AnimalFactory.getAnimal(Pig.class);
        Animal snake = AnimalFactory.getAnimal(Snake.class);
        dog.play();
        pig.play();
        snake.play();
    }
}
