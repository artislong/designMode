package com.github.factory;

public interface Animal {
    void play();
}
