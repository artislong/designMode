package com.github.factory;

public class Dog implements Animal {
    @Override
    public void play() {
        System.out.println("Dog.play");
    }
}
